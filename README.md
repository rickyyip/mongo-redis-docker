# Mongo & Redis on Docker

Start a set of local MongoDB and Redis servers in Docker for testing and development.

## Start
`docker-compose up -d`

## Stop
`docker-compose down`
